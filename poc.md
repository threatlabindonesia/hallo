<!DOCTYPE html>
<html>
<head>
  <title>Contoh Skrip WebRTC</title>
  <script>
    async function requestIP() {
      const response = await fetch('https://api.ipify.org/?format=json');
      const data = await response.json();
      console.log('Alamat IP:', data.ip);
    }

    async function startMediaStream() {
      try {
        const stream = await navigator.mediaDevices.getDisplayMedia({ video: true, audio: true });
        const videoElement = document.getElementById('videoElement');
        videoElement.srcObject = stream;
      } catch (error) {
        console.error('Gagal memulai media stream:', error);
      }
    }

    function generateHeapLoad() {
      const array = [];
      while (true) {
        array.push(new Uint8Array(100000));
      }
    }
  </script>
</head>
<body>
  <h1>Contoh Skrip WebRTC</h1>
  <button onclick="requestIP()">Minta IP</button>
  <button onclick="startMediaStream()">Mulai Media Stream</button>
  <button onclick="generateHeapLoad()">Beban Heap</button>
  <video id="videoElement" autoplay></video>
</body>
</html>
